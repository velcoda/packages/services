<?php

namespace Velcoda\Services\Enum;

enum App
{
    case VELCODA;
    case TELEBUTLER;
    case CENTRICS;
}
