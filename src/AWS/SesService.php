<?php

namespace Velcoda\Services\AWS;

use Aws\Credentials\Credentials;
use Aws\Exception\AwsException;
use Aws\Ses\SesClient;
use Aws\Sns\SnsClient;
use Velcoda\ApiAuth\Models\IdentityBase;
use Velcoda\Exceptions\Exceptions\HTTP_INTERNAL_SERVER;
use Velcoda\Services\Enum\App;
use Velcoda\Services\Models\SnsMessage;

class SesService
{
    protected SesClient $client;
    /**
     * CognitoClient constructor.
     *
     */
    public function __construct()
    {
        $credentials = new Credentials(env('AWS_ACCESS_KEY_ID'), env('AWS_SECRET_ACCESS_KEY'));
        $this->client = new SesClient([
            'version' => 'latest',
            'region' => env('AWS_DEFAULT_REGION'),
            'credentials' => $credentials,
        ]);
    }

    /**
     * @throws HTTP_INTERNAL_SERVER
     */
    public function send(App $app, string $from_email, string $from_name, array $to_emails, string|null $reply_to_email, string|null $configuration_set, string $subject, $html): string
    {
        $char_set = 'UTF-8';

        $body = [
            'Destination' => [
                'ToAddresses' => $to_emails,
            ],
            'Source' => $from_name . ' <' . $from_email . '>',
            'Message' => [
                'Body' => [
                    'Html' => [
                        'Charset' => $char_set,
                        'Data' => $html,
                    ],
                ],
                'Subject' => [
                    'Charset' => $char_set,
                    'Data' => $subject,
                ],
            ],
        ];
        if ($reply_to_email) {
            $body['ReplyToAddresses'] = [$reply_to_email];
        }
        if ($configuration_set) {
            $body['ConfigurationSetName'] = $configuration_set;
        }
        try {
            $result = $this->client->sendEmail($body);
            return $result['MessageId'];
        } catch (AwsException $e) {
            throw new HTTP_INTERNAL_SERVER($e->getAwsErrorMessage());
        }
    }
}
