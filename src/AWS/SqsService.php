<?php

namespace Velcoda\Services\AWS;

use Aws\Credentials\Credentials;
use Aws\Sns\SnsClient;
use Velcoda\ApiAuth\Models\IdentityBase;
use Velcoda\Services\Enum\App;
use Velcoda\Services\Models\SnsMessage;

class SnsService
{
    private $client;

    /**
     * CognitoClient constructor.
     *
     */
    public function __construct()
    {
        $credentials = new Credentials(env('AWS_ACCESS_KEY_ID'), env('AWS_SECRET_ACCESS_KEY'));
        $this->client = new SnsClient([
            'version'     => 'latest',
            'region'      => env('AWS_DEFAULT_REGION'),
            'credentials' => $credentials,
        ]);
    }

    public function publish($topic_arn, $subject, $body, App $app = App::TELEBUTLER) {
        $data = [
            'topic_arn' => $topic_arn,
            'subject' => $subject,
            'data' => $body,
            'app' => $app->name,
        ];
        $this->client->publish([
            'TopicArn' => $topic_arn,
            'Message' => json_encode($data),
            'Subject' => $subject
        ]);
    }

    public static function deconstruct($body) {
        return SnsMessage::deconstruct($body);
    }
}
