<?php

namespace Velcoda\Services;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;
use Velcoda\ApiAuth\Console\Commands\ActivateApiKey;
use Velcoda\ApiAuth\Console\Commands\DeactivateApiKey;
use Velcoda\ApiAuth\Console\Commands\DeleteApiKey;
use Velcoda\ApiAuth\Console\Commands\GenerateApiKey;
use Velcoda\ApiAuth\Console\Commands\ListApiKeys;
use Velcoda\ApiAuth\Http\Middleware\AuthorizeApiKey;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Velcoda\ApiAuth\Models\ApiKey;

class VelcodaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @param Router $router
     * @return void
     */
    public function boot(Router $router)
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/db_connections.php', 'database.connections');
    }
}
