<?php

namespace Velcoda\Services\DB\Velcoda;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Velcoda\Exceptions\Exceptions\HTTP_NOT_FOUND;
use Velcoda\Services\Http\BaseResponse;
use Velcoda\Services\Http\BaseService;
use Velcoda\Services\Models\IntCustomer;

class Customers
{
    /**
     * @throws HTTP_NOT_FOUND
     */
    public static function getById($id):IntCustomer {
        $customer = IntCustomer::find($id);
        if (!$customer) {
            throw new HTTP_NOT_FOUND('Customer not found');
        }
        return $customer;
    }

    // list all customer ids where My users customer is agent of
    /**
     *
     * returns list of all customers ids, the authenticated user is managing
     *
     * @return string[]
     */
    public static function allMyCustomers():array {
        $customer_id = Auth::user()->customer_id;
        return IntCustomer::where('agent_customer_id', '=', $customer_id)->orWhere('id', '=', $customer_id)->pluck('id')->toArray();
    }

    /**
     * returns list of all customers ids
     * @return string[]
     */
    public static function allCustomers():array {
        return IntCustomer::all()->pluck('id')->toArray();
    }
}
