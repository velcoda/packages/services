<?php

namespace Velcoda\Services\DB\Velcoda;

use Velcoda\ApiAuth\Models\IdentityBase;
use Velcoda\Exceptions\Exceptions\HTTP_NOT_FOUND;
use Velcoda\Services\Models\IntIdentity;

class Identities
{
    /**
     * @throws HTTP_NOT_FOUND
     */
    public static function getById(string $id): IdentityBase {
        $identity = IntIdentity::find($id);
        if (!$identity) {
            throw new HTTP_NOT_FOUND('Identity not found');
        }
        return $identity;
    }

    public static function getAllByCustomerId(string $customer_id) {
        return IntIdentity::where('customer_id', '=', $customer_id)->get();
    }
}
