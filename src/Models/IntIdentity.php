<?php

namespace Velcoda\Services\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Velcoda\ApiAuth\Models\IdentityBase;

class IntIdentity extends IdentityBase
{
    use HasFactory, SoftDeletes;

    protected $connection = 'int_identities';
    protected $table = 'identities';

}
