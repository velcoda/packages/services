<?php

namespace Velcoda\Services\Models;

use Velcoda\ApiAuth\Models\IdentityBase;
use Velcoda\Exceptions\Exceptions\HTTP_INTERNAL_SERVER;
use Velcoda\Services\Enum\App;

class SnsMessage
{
    public App $app;
    public array $data;
    public string $subject;
    public string $topic_arn;

    private function __construct()
    {
    }

    public static function deconstruct($body) {
        $msg = new self();
        $msg->data = $body['data'];
        $msg->topic_arn = $body['topic_arn'];
        $msg->subject = $body['subject'];
        $msg->app = self::parseApp($body['app']);
        return $msg;
    }

    private static function parseApp($app) {
        return match ($app) {
            App::CENTRICS->name => App::CENTRICS,
            App::TELEBUTLER->name => App::TELEBUTLER,
            default => throw new HTTP_INTERNAL_SERVER('App unknown'),
        };
    }
}
