<?php

namespace Velcoda\Services\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Velcoda\ApiAuth\Models\IdentityBase;

class IntCustomerSettings extends IdentityBase
{
    use HasFactory, SoftDeletes;

    protected $connection = 'int_customer_settings';
    protected $table = 'customer_settings';

}
