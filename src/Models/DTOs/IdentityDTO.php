<?php

namespace Velcoda\Services\Models\SNS\identities;

use Carbon\CarbonInterface;
use Illuminate\Support\Carbon;
use Velcoda\ApiAuth\Models\IdentityBase;
use Velcoda\Helpers\Changeset;
use Velcoda\Services\Models\SNS\BaseDTO;

final class DTOIdentity
{
    private string $id;
    private string $customer_id;
    private ?string $title_before;
    private string $first_name;
    private ?string $middle_names;
    private string $last_name;
    private ?string $title_after;
    private string $gender;
    private string $email;
    private ?CarbonInterface $email_verified_at;
    private ?string $avatar_url;
    private bool $is_admin;
    private bool $is_devops_admin;
    private bool $is_partner;
    private bool $is_recording_studio;
    private CarbonInterface $created_at;
    private CarbonInterface $updated_at;
    private ?CarbonInterface $deleted_at;

    public function __construct(IdentityBase $identity)
    {
        $this->id = $identity->id;
        $this->customer_id = $identity->customer_id;
        $this->title_before = $identity->title_before;
        $this->first_name = $identity->first_name;
        $this->middle_names = $identity->middle_names;
        $this->last_name = $identity->last_name;
        $this->title_after = $identity->title_after;
        $this->gender = $identity->gender;
        $this->email = $identity->email;
        $this->email_verified_at = $identity->email_verified_at ? Carbon::parse($identity->email_verified_at) : null;
        $this->avatar_url = $identity->avatar_url;
        $this->is_admin = $identity->is_admin;
        $this->is_devops_admin = $identity->is_devops_admin;
        $this->is_partner = $identity->is_partner;
        $this->is_recording_studio = $identity->is_recording_studio;
        $this->created_at = Carbon::parse($identity->created_at);
        $this->updated_at = Carbon::parse($identity->updated_at);
        $this->deleted_at = Carbon::parse($identity->deleted_at);
    }

    public function getId(): string {
        return $this->id;
    }

    public function getCustomerId(): string {
        return $this->customer_id;
    }

    public function getTitleBefore(): string {
        return $this->title_before;
    }

    public function getFirstName(): string {
        return $this->first_name;
    }

    public function getMiddleNames(): string {
        return $this->middle_names;
    }

    public function getLastName(): string {
        return $this->last_name;
    }

    public function getTitleAfter(): string {
        return $this->title_after;
    }

    public function getGender(): string {
        return $this->gender;
    }

    public function getEmail(): string {
        return $this->email;
    }

    public function getEmailVerifiedAt(): CarbonInterface {
        return $this->email_verified_at;
    }

    public function getAvatarUrl(): string {
        return $this->avatar_url;
    }

    public function isIsAdmin(): bool {
        return $this->is_admin;
    }

    public function isIsDevopsAdmin(): bool {
        return $this->is_devops_admin;
    }

    public function isIsPartner(): bool {
        return $this->is_partner;
    }

    public function isIsRecordingStudio(): bool {
        return $this->is_recording_studio;
    }

    public function getCreatedAt(): CarbonInterface {
        return $this->created_at;
    }

    public function getUpdatedAt(): CarbonInterface {
        return $this->updated_at;
    }

    public function getDeletedAt(): ?CarbonInterface {
        return $this->deleted_at;
    }
}