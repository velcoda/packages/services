<?php

namespace Velcoda\Services\Models\SNS;

use Velcoda\ApiAuth\Models\IdentityBase;
use Velcoda\Exceptions\Exceptions\HTTP_NOT_FOUND;
use Velcoda\Services\DB\Velcoda\Identities;

final class AbsenceNotYetActivatedMessageBody extends BaseMessageBody
{
    /**
     * @throws HTTP_NOT_FOUND
     */
    public function __construct($receiver_identity_id, string $entity_id, $start_time, $end_time, int $hours_before_start)
    {
        parent::__construct($receiver_identity_id);
        $this->addKey('start_time', $start_time);
        $this->addKey('end_time', $end_time);
        $this->addKey('entity_id', $entity_id);
        $this->addKey('hours_before_start', $hours_before_start);
    }

    /**
     * @throws HTTP_NOT_FOUND
     */
    public static function fromArray(array $body): AbsenceNotYetActivatedMessageBody
    {
        return new self(
            $body['receiver_identity']['id'],
            $body['entity_id'],
            $body['start_time'],
            $body['end_time'],
            $body['hours_before_start'],
        );
    }

    public function getStartTime() {
        return $this->args['start_time'];
    }

    public function getEndTime() {
        return $this->args['end_time'];
    }

    public function getHoursBeforeStart() {
        return $this->args['hours_before_start'];
    }
    public function getEntityId() {
        return $this->args['entity_id'];
    }
}