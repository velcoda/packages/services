<?php

namespace Velcoda\Services\Models\SNS;

use Velcoda\ApiAuth\Models\IdentityBase;
use Velcoda\Exceptions\Exceptions\HTTP_NOT_FOUND;
use Velcoda\Services\DB\Velcoda\Identities;

class BaseMessageBody
{
    private IdentityBase $receiver_identity;
    private ?IdentityBase $actor_identity = null;

    protected array $args = [];


    /**
     * @throws HTTP_NOT_FOUND
     */
    public function __construct(string $receiver_identity_id, $actor_identity_id = null)
    {
        $this->receiver_identity = Identities::getById($receiver_identity_id);
        if ($actor_identity_id) {
            $this->actor_identity = Identities::getById($actor_identity_id);
        }
    }

    public static function fromArray(array $body): BaseMessageBody {
        $actor_identity = null;
        if (array_key_exists('actor_identity', $body)) {
            $actor_identity = $body['actor_identity']['id'];
        }
        $s = new self($body['receiver_identity']['id'], $actor_identity);
        $s->args = $body;
        return $s;
    }

    /**
     * @throws HTTP_NOT_FOUND
     */
    public function getReceiverIdentity(): ?IdentityBase {
        return $this->receiver_identity;
    }

    /**
     * @throws HTTP_NOT_FOUND
     */
    public function getActorIdentity(): ?IdentityBase {
        if ($this->actor_identity) {
            return $this->actor_identity;
        }
        if (array_key_exists('actor_identity_id', $this->args)) {
            return Identities::getById($this->args['actor_identity_id']);
        }
        return null;
    }

    public function addKey($key, $value)
    {
        $this->args[$key] = $value;
    }

    public function toArray(): array
    {
        $data = [
            'receiver_identity' => $this->receiver_identity->toArray()
        ];
        if (isset($this->actor_identity) && $this->actor_identity) {
            $data['actor_identity'] = $this->actor_identity->toArray();
        }
        foreach ($this->args as $key=>$value) {
            if (!in_array($key, ['receiver_identity', 'actor_identity'])) {
                $data[$key] = $value;
            }
        }
        return $data;
    }
}
