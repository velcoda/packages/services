<?php

namespace Velcoda\Services\Models\SNS;

final class HelpMessageBody extends BaseMessageBody
{
    public function __construct($receiver_identity_id)
    {
        parent::__construct($receiver_identity_id);
    }
}