<?php

namespace Velcoda\Services\Models\SNS;

use Velcoda\Exceptions\Exceptions\HTTP_NOT_FOUND;

final class AbsenceStartMessageBody extends BaseMessageBody
{
    public function __construct($receiver_identity_id, string $entity_id, $start_time, $end_time)
    {
        parent::__construct($receiver_identity_id);
        $this->addKey('start_time', $start_time);
        $this->addKey('end_time', $end_time);
        $this->addKey('entity_id', $entity_id);
    }

    public function getStartTime() {
        return $this->args['start_time'];
    }

    public function getEndTime() {
        return $this->args['end_time'];
    }

    public function getEntityId() {
        return $this->args['entity_id'];
    }

    /**
     * @throws HTTP_NOT_FOUND
     */
    public static function fromArray(array $body): AbsenceStartMessageBody
    {
        return new self(
            $body['receiver_identity']['id'],
            $body['entity_id'],
            $body['start_time'],
            $body['end_time'],
        );
    }
}