<?php

namespace Velcoda\Services\Models\SNS;

use Velcoda\Exceptions\Exceptions\HTTP_NOT_FOUND;

final class VoiceCallCreatedMessageBody extends BaseMessageBody
{
    public function __construct(?string $vta_type, string $receiver_identity_id, string $entity_id, ?string $caller_name, ?string $caller_number, float|int|string $timestamp, array $receiver_emails, array $receiver_sms, $voicemail_url = null)
    {
        parent::__construct($receiver_identity_id);

        $this->addKey('vta_type', $vta_type);
        $this->addKey('entity_id', $entity_id);
        $this->addKey('caller_number', $caller_number);
        $this->addKey('caller_name', $caller_name);
        $this->addKey('timestamp', $timestamp);
        $this->addKey('receiver_sms', $receiver_sms);
        $this->addKey('receiver_emails', $receiver_emails);
        $this->addKey('voicemail_url', $voicemail_url);
    }

    public function getCallerNumber(): string {
        return $this->args['caller_number'];
    }
    public function getCallerName(): ?string{
        return $this->args['caller_name'];
    }

    public function getTimestamp(): float|int|string {
        return $this->args['timestamp'];
    }

    public function getVoicemailUrl():?string {
        return $this->args['voicemail_url'];
    }

    public function getVtaType():?string {
        return $this->args['vta_type'];
    }

    public function getReceiverSMS():array {
        return $this->args['receiver_sms'];
    }

    public function getReceiverEmail():array {
        return $this->args['receiver_emails'];
    }

    public function getEntityId():string {
        return $this->args['entity_id'];
    }

    /**
     * @throws HTTP_NOT_FOUND
     */
    public static function fromArray(array $body): VoiceCallCreatedMessageBody
    {
        return new self(
            $body['vta_type'],
            $body['receiver_identity']['id'],
            $body['entity_id'],
            $body['caller_name'],
            $body['caller_number'],
            $body['timestamp'],
            $body['receiver_emails'],
            $body['receiver_sms'],
            $body['voicemail_url']
        );
    }
}
