<?php

namespace Velcoda\Services\Models\SNS;

use Velcoda\Exceptions\Exceptions\HTTP_NOT_FOUND;

final class PasswordResetMessageBody extends BaseMessageBody
{
    public function __construct($receiver_identity_id, $token)
    {
        parent::__construct($receiver_identity_id);
        $this->addKey('token', $token);
    }

    public function getToken() {
        return $this->args['token'];
    }

    /**
     * @throws HTTP_NOT_FOUND
     */
    public static function fromArray(array $body): PasswordResetMessageBody
    {
        return new self(
            $body['receiver_identity']['id'],
            $body['token']
        );
    }
}