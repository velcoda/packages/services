<?php

namespace Velcoda\Services\Models\SNS;

final class AbsenceEndMessageBody extends BaseMessageBody
{
    public function __construct($receiver_identity_id, string $entity_id)
    {
        parent::__construct($receiver_identity_id);
        $this->addKey('entity_id', $entity_id);
    }

    public static function fromArray(array $body): AbsenceEndMessageBody
    {
        return new self(
            $body['receiver_identity']['id'],
            $body['entity_id'],
        );
    }

    public function getEntityId() {
        return $this->args['entity_id'];
    }
}