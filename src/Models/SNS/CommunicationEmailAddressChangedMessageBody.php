<?php

namespace Velcoda\Services\Models\SNS;

use Velcoda\Exceptions\Exceptions\HTTP_NOT_FOUND;

final class CommunicationEmailAddressChangedMessageBody extends BaseMessageBody
{
    public function __construct($receiver_identity_id, $actor_identity_id, $email_address_added)
    {
        parent::__construct($receiver_identity_id, $actor_identity_id);
        $this->addKey('email_address_added', $email_address_added);
    }

    public function getEmailAddressAdded() {
        return $this->args['email_address_added'];
    }

    /**
     * @throws HTTP_NOT_FOUND
     */
    public static function fromArray(array $body): CommunicationEmailAddressChangedMessageBody
    {
        return new self(
            $body['receiver_identity']['id'],
            $body['actor_identity']['id'],
            $body['email_address_added']
        );
    }
}