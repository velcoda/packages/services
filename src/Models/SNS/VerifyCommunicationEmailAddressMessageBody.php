<?php

namespace Velcoda\Services\Models\SNS;

use Velcoda\Exceptions\Exceptions\HTTP_NOT_FOUND;

final class VerifyCommunicationEmailAddressMessageBody extends BaseMessageBody
{
    public function __construct($receiver_identity_id, $actor_identity_id, $address_to_be_added, $token)
    {
        parent::__construct($receiver_identity_id, $actor_identity_id);
        $this->addKey('address_to_be_added', $address_to_be_added);
        $this->addKey('token', $token);
    }

    public function getAddressToBeAdded() {
        return $this->args['address_to_be_added'];
    }

    public function getToken() {
        return $this->args['token'];
    }

    /**
     * @throws HTTP_NOT_FOUND
     */
    public static function fromArray(array $body): VerifyCommunicationEmailAddressMessageBody
    {
        return new self(
            $body['receiver_identity']['id'],
            $body['actor_identity']['id'],
            $body['address_to_be_added'],
            $body['token'],
        );
    }
}