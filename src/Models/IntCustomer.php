<?php

namespace Velcoda\Services\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Velcoda\ApiAuth\Helpers\CommonAuthorizationRules;

class IntCustomer extends Model
{
    use HasFactory, SoftDeletes;

    protected $connection = 'int_customers';
    protected $table = 'customers';

    public $incrementing = false;
    protected $keyType = 'string';

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'allow_sms_notifications' => 'boolean',
        'allow_vat_pin_deactivation' => 'boolean',
        'allow_chain_announcements' => 'boolean',
        'allow_edit_announcement_activation_time' => 'boolean',
        'is_demo' => 'boolean',
        'country_id' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
        'deleted_at' => 'timestamp',
    ];
}
