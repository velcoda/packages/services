<?php

namespace Velcoda\Services\Http\Velcoda;

use Velcoda\Services\Http\BaseResponse;
use Velcoda\Services\Http\BaseService;

class NotificationGateway extends BaseService
{
    const SERVICE_NAME = 'notification-gateway';

    public static function client($timeout = 6): NotificationGateway
    {
        return new NotificationGateway(self::SERVICE_NAME, $timeout);
    }
}
