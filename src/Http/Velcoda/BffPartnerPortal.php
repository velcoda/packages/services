<?php

namespace Velcoda\Services\Http\Velcoda;

use Velcoda\Services\Http\BaseResponse;
use Velcoda\Services\Http\BaseService;

class BffPartnerPortal extends BaseService
{
    const SERVICE_NAME = 'bff-partner-portal';

    public static function client($timeout = 6): BffPartnerPortal
    {
        return new BffPartnerPortal(self::SERVICE_NAME, $timeout);
    }
}
