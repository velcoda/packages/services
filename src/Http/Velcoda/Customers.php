<?php

namespace Velcoda\Services\Http\Velcoda;

use Illuminate\Support\Facades\DB;
use Velcoda\Services\Http\BaseResponse;
use Velcoda\Services\Http\BaseService;
use Velcoda\Services\Models\IntCustomer;

class Customers extends BaseService
{
    const SERVICE_NAME = 'customers';

    public static function client($timeout = 6): Customers
    {
        return new Customers(self::SERVICE_NAME, $timeout);
    }

    public function listCountries(): BaseResponse
    {
        return $this->get('/' . self::SERVICE_NAME . '/v1/countries');
    }

    public function getCustomerById($customer_id): BaseResponse
    {
        return $this->get('/' . self::SERVICE_NAME . '/v1/customers/' . $customer_id);
    }

    public function updateCustomer(
        $customer_id,
        $name = null,
        $street = null,
        $street2 = null,
        $zip = null,
        $city = null,
        $country_id = null,
        $velcoda_price = null,
        $agent_customer_id = null,
        $agent_price = null
    ): BaseResponse
    {
        $body = [];
        if ($name) $body['name'] = $name;
        if ($street) $body['street'] = $street;
        if ($street2) $body['street2'] = $street2;
        if ($zip) $body['zip'] = $zip;
        if ($city) $body['city'] = $city;
        if ($country_id) $body['country_id'] = $country_id;
        if ($velcoda_price !== null) $body['velcoda_price'] = $velcoda_price;
        if ($agent_customer_id) $body['agent_customer_id'] = $agent_customer_id;
        if ($agent_price !== null) $body['agent_price'] = $agent_price;
        return $this->patch('/' . self::SERVICE_NAME . '/v1/customers/' . $customer_id, $body);
    }

    public function listCustomers($page, $limit, $order_by, $sort = 'asc', $search = null): BaseResponse
    {
        $params = [];
        if ($search) {
            $params[] = 'search=' . $search;
        }
        if ($page) {
            $params[] = 'page=' . $page;
        }
        if ($order_by) {
            $params[] = 'order_by=' . $order_by;
        }
        if ($sort) {
            $params[] = 'sort=' . $sort;
        }
        if ($limit) {
            $params[] = 'limit=' . $limit;
        }
        return $this->get('/' . self::SERVICE_NAME . '/v1/customers?' . join('&', $params));
    }

    public function listIdentitiesByCustomerId($customer_id): BaseResponse
    {
        return $this->get('/' . self::SERVICE_NAME . '/v1/customers/' . $customer_id . '/identities');
    }

    public function listIdentitiesByCustomerIdApi($customer_id): BaseResponse
    {
        return $this->get('/' . self::SERVICE_NAME . '/v1/api-key/customers/' . $customer_id . '/identities');
    }

    public function getCustomerByIdApi($customer_id): BaseResponse
    {
        return $this->get('/' . self::SERVICE_NAME . '/v1/api-key/customers/' . $customer_id);
    }
}
