<?php

namespace Velcoda\Services\Http\Velcoda;

use Carbon\CarbonInterface;
use Velcoda\Services\Http\BaseResponse;
use Velcoda\Services\Http\BaseService;

class Identities  extends BaseService
{
    const SERVICE_NAME = 'identities';

    public static function client($timeout = 6)
    {
        return new Identities(self::SERVICE_NAME, $timeout);
    }

    public function getIdentityById($identity_id): BaseResponse {
        return $this->get('/' . self::SERVICE_NAME . '/v1/identities/' . $identity_id);
    }

    public function listIdentities($page = 1, $limit = 15, $sort = 'asc', $order_by = 'last_name', $search = null, $filters = []): BaseResponse
    {
        $qs = [];
        foreach ($filters as $key => $value) {
            $qs[] = 'filter[' . $key . ']=' . $value;
        }
        $qs[] = 'page=' . $page;
        $qs[] = 'limit=' . $limit;
        if ($sort) $qs[] = 'sort=' . $sort;
        if ($order_by) $qs[] = 'order_by=' . $order_by;
        if ($search) $qs[] = 'search=' . $search;

        $query_string = implode('&', $qs);
        if (count($qs) > 0) {
            $query_string = '?' . $query_string;
        }
        return $this->get('/' . self::SERVICE_NAME . '/v1/identities' . $query_string);
    }

    public function listIdentitiesByApi($page = 1, $limit = 15, $sort = 'asc', $order_by = 'last_name', $search = null, $filters = []): BaseResponse
    {
        $qs = [];
        foreach ($filters as $key => $value) {
            $qs[] = 'filter[' . $key . ']=' . $value;
        }
        $qs[] = 'page=' . $page;
        $qs[] = 'limit=' . $limit;
        if ($sort) $qs[] = 'sort=' . $sort;
        if ($order_by) $qs[] = 'order_by=' . $order_by;
        if ($search) $qs[] = 'search=' . $search;

        $query_string = implode('&', $qs);
        if (count($qs) > 0) {
            $query_string = '?' . $query_string;
        }
        return $this->get('/' . self::SERVICE_NAME . '/v1/api-key/identities' . $query_string);
    }

    public function updateIdentity(
        $identity_id,
        $title_before = null,
        $first_name = null,
        $middle_names = null,
        $last_name = null,
        $title_after = null,
        $email = null,
        $gender = null,
    ): BaseResponse
    {
        $body = [];
        if ($title_before) $body['title_before'] = $title_before;
        if ($first_name) $body['first_name'] = $first_name;
        if ($middle_names) $body['middle_names'] = $middle_names;
        if ($last_name) $body['last_name'] = $last_name;
        if ($title_after) $body['title_after'] = $title_after;
        if ($email) $body['email'] = $email;
        if ($gender) $body['gender'] = $gender;

        return $this->patch('/' . self::SERVICE_NAME . '/v1/identities/' . $identity_id, $body);
    }

    public function auth($email, $password, $client_id, $client_secret, $scopes = 'telebutler_user'): BaseResponse {
        $body = [
            'grant_type' => 'password',
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'scope' => $scopes,
            'username' => $email,
            'password' => $password
        ];
        return $this->post('/' . self::SERVICE_NAME . '/v1/oauth/token', $body);
    }

    public function refresh($refresh_token, $client_id, $client_secret, $scopes = '*'): BaseResponse {
        $body = [
            'grant_type' => 'refresh_token',
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'scope' => $scopes,
            'refresh_token' => $refresh_token
        ];
        return $this->post('/' . self::SERVICE_NAME . '/v1/oauth/token', $body);
    }

    public function getIdentityByIdApi($identity_id): BaseResponse {
        return $this->get('/' . self::SERVICE_NAME . '/v1/api-key/identities/' . $identity_id);
    }

    public function createPatForIdentity($identity_id): BaseResponse {
        return $this->post('/' . self::SERVICE_NAME . '/v1/identities/' . $identity_id . '/pat');
    }

    public function createPatForIdentityApi($identity_id, string $title, CarbonInterface|null $expires, array $scopes): BaseResponse {
        $body = [
            'title' => $title,
            'scopes' => $scopes
        ];
        if ($expires) {
            $body['expires_at'] = $expires->toDateTimeString();
        }
        return $this->post('/' . self::SERVICE_NAME . '/v1/api-key/identities/' . $identity_id . '/pat', $body);
    }
}
