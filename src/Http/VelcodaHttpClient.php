<?php

namespace Velcoda\Services\Http;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class VelcodaHttpClient extends Client
{
    public function request(string $method, $uri = '', array $options = []): ResponseInterface
    {
        return inspector()->addSegment(function () use ($method, $uri, $options) {
            return parent::request($method, $uri, $options);
        }, "http", "{$method} {$uri}", true);
    }
}