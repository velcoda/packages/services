<?php

namespace Velcoda\Services\Http;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

class BaseResponse
{
    private ResponseInterface $response;

    public function __construct(ResponseInterface $response)
    {
        $this->response = $response;
    }

    public function body() {
        return json_decode($this->response->getBody()->getContents());
    }

    public function headers() {
        return $this->response->getHeaders();
    }

    public function statusCode() {
        return $this->response->getStatusCode();
    }

    public function response() {
        return $this->response;
    }
}
