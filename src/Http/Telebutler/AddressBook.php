<?php

namespace Velcoda\Services\Http\Telebutler;

use GuzzleHttp\Exception\GuzzleException;
use Velcoda\Exceptions\Exceptions\HTTP_BAD_REQUEST;
use Velcoda\Exceptions\Exceptions\HTTP_CONFLICT;
use Velcoda\Exceptions\Exceptions\HTTP_FORBIDDEN;
use Velcoda\Exceptions\Exceptions\HTTP_INTERNAL_SERVER;
use Velcoda\Exceptions\Exceptions\HTTP_NOT_FOUND;
use Velcoda\Exceptions\Exceptions\HTTP_PAYMENT_REQUIRED;
use Velcoda\Exceptions\Exceptions\HTTP_UNAUTHORIZED;
use Velcoda\Exceptions\Exceptions\HTTP_UNPROCESSABLE_ENTITY;
use Velcoda\Services\Http\BaseResponse;
use Velcoda\Services\Http\BaseService;

class AddressBook extends BaseService
{
    const SERVICE_NAME = 'address-book';

    public static function client($timeout = 6): AddressBook
    {
        return new AddressBook(self::SERVICE_NAME, $timeout);
    }

    /**
     * @throws HTTP_PAYMENT_REQUIRED
     * @throws HTTP_CONFLICT
     * @throws HTTP_NOT_FOUND
     * @throws HTTP_UNAUTHORIZED
     * @throws HTTP_BAD_REQUEST
     * @throws HTTP_INTERNAL_SERVER
     * @throws HTTP_FORBIDDEN
     * @throws GuzzleException
     * @throws HTTP_UNPROCESSABLE_ENTITY
     */
    public function listContacts(string $identity_id): ?BaseResponse
    {
        $url = '/v1';
        if ($this->api_key) {
            $url .= '/api-key';
        }
        $url .= '/identities/' . $identity_id . '/contacts';
        return $this->get("/" . self::SERVICE_NAME . $url);
    }

    public function showContacts(string $identity_id, string $contact_id): ?BaseResponse
    {
        $url = '/v1';
        if ($this->api_key) {
            $url .= '/api-key';
        }
        $url .= '/identities/' . $identity_id . '/contacts/' . $contact_id;
        return $this->get("/" . self::SERVICE_NAME . $url);
    }

    public function upsertContact(string $identity_id, string $contact_id, string $name): ?BaseResponse
    {
        $url = '/v1';
        if ($this->api_key) {
            $url .= '/api-key';
        }
        $url .= '/identities/' . $identity_id . '/contacts/' . $contact_id;
        return $this->patch("/" . self::SERVICE_NAME . $url, ['name' => $name]);
    }

    /**
     * @throws HTTP_PAYMENT_REQUIRED
     * @throws HTTP_CONFLICT
     * @throws HTTP_NOT_FOUND
     * @throws HTTP_UNAUTHORIZED
     * @throws HTTP_BAD_REQUEST
     * @throws HTTP_FORBIDDEN
     * @throws HTTP_INTERNAL_SERVER
     * @throws GuzzleException
     * @throws HTTP_UNPROCESSABLE_ENTITY
     */
    public function createContactEmail(string $identity_id, string $contact_id, string $email): ?BaseResponse
    {
        $url = '/v1';
        if ($this->api_key) {
            $url .= '/api-key';
        }
        $url .= '/identities/' . $identity_id . '/contacts/' . $contact_id . '/email-addresses';
        return $this->post("/" . self::SERVICE_NAME . $url, ['email_address' => $email]);
    }

    /**
     * @throws HTTP_CONFLICT
     * @throws HTTP_PAYMENT_REQUIRED
     * @throws HTTP_NOT_FOUND
     * @throws HTTP_UNAUTHORIZED
     * @throws HTTP_BAD_REQUEST
     * @throws HTTP_FORBIDDEN
     * @throws HTTP_INTERNAL_SERVER
     * @throws GuzzleException
     */
    public function deleteContactEmail(string $identity_id, string $contact_id, string $email_id): ?BaseResponse
    {
        $url = '/v1';
        if ($this->api_key) {
            $url .= '/api-key';
        }
        $url .= '/identities/' . $identity_id . '/contacts/' . $contact_id . '/email-addresses/' . $email_id;
        return $this->delete("/" . self::SERVICE_NAME . $url);
    }

    /**
     * @throws HTTP_PAYMENT_REQUIRED
     * @throws HTTP_CONFLICT
     * @throws HTTP_NOT_FOUND
     * @throws HTTP_UNAUTHORIZED
     * @throws HTTP_BAD_REQUEST
     * @throws HTTP_FORBIDDEN
     * @throws HTTP_INTERNAL_SERVER
     * @throws GuzzleException
     * @throws HTTP_UNPROCESSABLE_ENTITY
     */
    public function createContactPhone(string $identity_id, string $contact_id, string $phone): ?BaseResponse
    {
        $url = '/v1';
        if ($this->api_key) {
            $url .= '/api-key';
        }
        $url .= '/identities/' . $identity_id . '/contacts/' . $contact_id . '/phone-numbers';
        return $this->post("/" . self::SERVICE_NAME . $url, ['phone_number' => $phone]);
    }

    /**
     * @throws HTTP_PAYMENT_REQUIRED
     * @throws HTTP_CONFLICT
     * @throws HTTP_NOT_FOUND
     * @throws HTTP_UNAUTHORIZED
     * @throws HTTP_BAD_REQUEST
     * @throws HTTP_INTERNAL_SERVER
     * @throws HTTP_FORBIDDEN
     * @throws GuzzleException
     */
    public function deleteContactPhone(string $identity_id, string $contact_id, string $phone_id): ?BaseResponse
    {
        $url = '/v1';
        if ($this->api_key) {
            $url .= '/api-key';
        }
        $url .= '/identities/' . $identity_id . '/contacts/' . $contact_id . '/phone-numbers/' . $phone_id;
        return $this->delete("/" . self::SERVICE_NAME . $url);
    }
}
