<?php

namespace Velcoda\Services\Http\Telebutler;

use Velcoda\Services\Http\BaseService;

class BffTelebutler extends BaseService
{
    const SERVICE_NAME = 'bff-telebutler';

    public static function client($timeout = 6): BffTelebutler
    {
        return new BffTelebutler(self::SERVICE_NAME, $timeout);
    }
}
