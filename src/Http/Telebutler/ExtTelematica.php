<?php

namespace Velcoda\Services\Http\Telebutler;

use GuzzleHttp\Exception\GuzzleException;
use Velcoda\Exceptions\Exceptions\HTTP_BAD_REQUEST;
use Velcoda\Exceptions\Exceptions\HTTP_CONFLICT;
use Velcoda\Exceptions\Exceptions\HTTP_FORBIDDEN;
use Velcoda\Exceptions\Exceptions\HTTP_INTERNAL_SERVER;
use Velcoda\Exceptions\Exceptions\HTTP_NOT_FOUND;
use Velcoda\Exceptions\Exceptions\HTTP_PAYMENT_REQUIRED;
use Velcoda\Exceptions\Exceptions\HTTP_UNAUTHORIZED;
use Velcoda\Exceptions\Exceptions\HTTP_UNPROCESSABLE_ENTITY;
use Velcoda\Services\Http\BaseResponse;
use Velcoda\Services\Http\BaseService;

class ExtTelematica extends BaseService
{
    const SERVICE_NAME = 'ext-telematica';

    public static function client($timeout = 6): ExtTelematica
    {
        return new ExtTelematica(self::SERVICE_NAME, $timeout);
    }

    public function loadMessageReceiver($identity_id): BaseResponse {
        $url = '/v1';
        if ($this->api_key) {
            $url .= '/api-key';
        }
        $url .= '/identities/' . $identity_id . '/message-receivers';
        return $this->get('/' . self::SERVICE_NAME . $url);
    }

    public function deleteMessageReceiver($identity_id, $receiver_id): BaseResponse {
        $url = '/v1';
        if ($this->api_key) {
            $url .= '/api-key';
        }
        $url .= '/identities/' . $identity_id . '/message-receivers/' . $receiver_id;
        return $this->delete('/' . self::SERVICE_NAME . $url);
    }

    public function createMessageReceiver($identity_id, $name, $email = null, $phone = null): BaseResponse {
        $url = '/v1';
        if ($this->api_key) {
            $url .= '/api-key';
        }
        $url .= '/identities/' . $identity_id . '/message-receivers';

        $body = [
            'name' => $name,
            'email_address' => $email,
            'phone_number' => $phone,

        ];
        return $this->post('/' . self::SERVICE_NAME . $url, $body);
    }

    /**
     * @throws HTTP_PAYMENT_REQUIRED
     * @throws HTTP_CONFLICT
     * @throws HTTP_NOT_FOUND
     * @throws HTTP_UNAUTHORIZED
     * @throws HTTP_BAD_REQUEST
     * @throws HTTP_FORBIDDEN
     * @throws HTTP_INTERNAL_SERVER
     * @throws GuzzleException
     * @throws HTTP_UNPROCESSABLE_ENTITY
     */
    public function upsertVta($identity_id, $pin, $management_number, $base_number, $extension, $action_after_out_of_office_msg, $action_after_default_msg): BaseResponse {
        $url = '/v1';
        if ($this->api_key) {
            $url .= '/api-key';
        }
        $url .= '/identities/' . $identity_id . '/vtas';
        $body = [];
        if ($pin) {
            $body['pin'] = $pin;
        }
        if ($management_number) {
            $body['management_number'] = $management_number;
        }
        if ($base_number) {
            $body['base_number'] = $base_number;
        }
        if ($extension) {
            $body['extension'] = $extension;
        }
        if ($action_after_default_msg) {
            $body['action_after_default_msg'] = self::checkAction($action_after_default_msg);
        }
        if ($action_after_out_of_office_msg) {
            $body['action_after_out_of_office_msg'] = self::checkAction($action_after_out_of_office_msg);
        }
        return $this->patch('/' . self::SERVICE_NAME . $url, $body);
    }


    /**
     * @throws HTTP_PAYMENT_REQUIRED
     * @throws HTTP_CONFLICT
     * @throws HTTP_NOT_FOUND
     * @throws HTTP_UNAUTHORIZED
     * @throws HTTP_BAD_REQUEST
     * @throws HTTP_FORBIDDEN
     * @throws HTTP_INTERNAL_SERVER
     * @throws GuzzleException
     * @throws HTTP_UNPROCESSABLE_ENTITY
     */
    public function rolloutAudiofile($identity_id, $type, $path): BaseResponse {

        if (!in_array($type, ['standard', 'absence'])) {
            throw new HTTP_BAD_REQUEST();
        }

        $url = '/v1/api-key/identities/' . $identity_id . '/vtas/rollout';

        $body = [
            'type' => $type,
            'url' => $path,
        ];
        return $this->post('/' . self::SERVICE_NAME . $url, $body);
    }

    /**
     * @throws HTTP_PAYMENT_REQUIRED
     * @throws HTTP_CONFLICT
     * @throws HTTP_NOT_FOUND
     * @throws HTTP_UNAUTHORIZED
     * @throws HTTP_BAD_REQUEST
     * @throws HTTP_FORBIDDEN
     * @throws HTTP_INTERNAL_SERVER
     * @throws GuzzleException
     * @throws HTTP_UNPROCESSABLE_ENTITY
     */
    public function migrateVta($identity_id, $mailbox_id, $pin, $management_number, $base_number, $extension, $action_after_out_of_office_msg, $action_after_default_msg): BaseResponse {
        $url = '/v1/api-key/identities/' . $identity_id . '/vtas';
        $body = [];

        $body['mailbox_id'] = $mailbox_id;
        $body['pin'] = $pin;
        $body['management_number'] = $management_number;
        $body['base_number'] = $base_number;
        $body['extension'] = $extension;
        $body['action_after_default_msg'] = self::checkAction($action_after_default_msg);
        $body['action_after_out_of_office_msg'] = self::checkAction($action_after_out_of_office_msg);

        return $this->post('/' . self::SERVICE_NAME . $url, $body);
    }

    /**
     * @throws HTTP_BAD_REQUEST
     */
    private function checkAction($action): string {
        if (!in_array($action, ['RECORD', 'HANGUP'])) {
            throw new HTTP_BAD_REQUEST('Action must be RECORD or HANGUP');
        }
        return $action;
    }

    public function showVta($identity_id): BaseResponse {
        $url = '/v1';
        if ($this->api_key) {
            $url .= '/api-key';
        }
        $url .= '/identities/' . $identity_id . '/vtas';

        return $this->get('/' . self::SERVICE_NAME . $url);
    }

    public function deleteVta($identity_id): BaseResponse {
        $url = '/v1';
        if ($this->api_key) {
            $url .= '/api-key';
        }
        $url .= '/identities/' . $identity_id . '/vtas';

        return $this->delete('/' . self::SERVICE_NAME . $url);
    }
}
