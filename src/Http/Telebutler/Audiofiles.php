<?php

namespace Velcoda\Services\Http\Telebutler;

use Illuminate\Http\UploadedFile;
use Velcoda\Services\Http\BaseResponse;
use Velcoda\Services\Http\BaseService;
use Velcoda\Services\Models\IntCustomerSettings;
use Velcoda\Services\Models\IntIdentity;

class Audiofiles extends BaseService
{
    const SERVICE_NAME = 'audiofiles';

    public static function client($timeout = 6): Audiofiles
    {
        return new Audiofiles(self::SERVICE_NAME, $timeout);
    }

    public function uploadSubstituteFile(UploadedFile $file, $filename, $customer_id, $identiy_id = null): BaseResponse {
        $url = '/v1/api-key/customers/' . $customer_id . '/speech-files/substitute-files';
        if ($identiy_id) {
            $url = $url . '?identity_id=' . $identiy_id;
        }
        $multipart = [
            [
                'name' => 'file',
                'filename' => $file->getClientOriginalName(),
                'contents' => fopen($file->getRealPath(), 'r')
            ],
            [
                'name'     => 'title',
                'contents' => $filename
            ],
            [
                'name'     => 'comment',
                'contents' => null
            ],
        ];
        return $this->request('POST', '/' . self::SERVICE_NAME . $url, [], $multipart, 'multipart/form-data');
    }

    public function createAbsenceFile($customer_id, $identity_id, $start_time, $end_time, $substitute_file_id = null, $explicit_time_configuration = false):BaseResponse {
        $url = '/v1/api-key/customers/' . $customer_id . '/identities/' . $identity_id . '/absence-announcement';

        $body = [
            'start_time' => $start_time,
            'end_time' => $end_time,
            'explicit_time_configuration' => $explicit_time_configuration
        ];
        if ($substitute_file_id) {
            $body['substitute_file_id'] = $substitute_file_id;
        }
        return $this->post('/' . self::SERVICE_NAME . $url, $body);
    }

    public function getStandardAnnouncement($customer_id, $identity_id):BaseResponse {
        $url = '/v1';
        if ($this->api_key) {
            $url .= '/api-key';
        }
        $url .= '/customers/' . $customer_id . '/identities/' . $identity_id . '/standard-announcement';
        return $this->get('/' . self::SERVICE_NAME . $url);
    }

    public function getDemoAnnouncement($customer_id, $identity_id):BaseResponse {
        $url = '/v1';
        if ($this->api_key) {
            $url .= '/api-key';
        }
        $url .= '/customers/' . $customer_id . '/identities/' . $identity_id . '/demo-announcement';
        return $this->get('/' . self::SERVICE_NAME . $url);
    }

    public function regenerateDemoAnnouncement($customer_id, $identity_id):BaseResponse {
        $url = '/v1';
        if ($this->api_key) {
            $url .= '/api-key';
        }
        $url .= '/customers/' . $customer_id . '/identities/' . $identity_id . '/demo-announcement';
        return $this->delete('/' . self::SERVICE_NAME . $url);
    }

    public function getStandardAnnouncementApi($customer_id, $identity_id):BaseResponse {
        $url = '/v1/api-key/customers/' . $customer_id . '/identities/' . $identity_id . '/standard-announcement';
        return $this->get('/' . self::SERVICE_NAME . $url);
    }

    public function createAbsenceFilesAsync($customer_id, $identity_id, $datas = []):array {
        $url = '/v1/api-key/customers/' . $customer_id . '/identities/' . $identity_id . '/absence-announcement';

        $promises = [];
        foreach ($datas as $key=>$data) {
            $start_time = $data['start_time'];
            $end_time = $data['end_time'];
            $substitute_file_id = $data['substitute_file_id']??null;
            $explicit_time_configuration = $data['explicit_time_configuration']??false;

            $body = [
                'start_time' => $start_time,
                'end_time' => $end_time,
                'explicit_time_configuration' => $explicit_time_configuration
            ];
            if ($substitute_file_id) {
                $body['substitute_file_id'] = $substitute_file_id;
            }
            $promises[$key] = $this->requestAsync('POST', '/' . self::SERVICE_NAME . $url, [], $body);
        }
        return $promises;
    }

    public function getFileByIdApi($customer_id, $file_id):BaseResponse {
        $url = '/v1/api-key/customers/' . $customer_id . '/speech-files/substitute-files/' . $file_id;
        return $this->get('/' . self::SERVICE_NAME . $url);
    }
}
