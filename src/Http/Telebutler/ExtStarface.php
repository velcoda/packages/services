<?php

namespace Velcoda\Services\Http\Telebutler;

use Velcoda\Services\Http\BaseResponse;
use Velcoda\Services\Http\BaseService;

class ExtStarface extends BaseService
{
    const SERVICE_NAME = 'ext-starface';

    public static function client($timeout = 6): ExtStarface
    {
        return new ExtStarface(self::SERVICE_NAME, $timeout);
    }

    public function loadSetting($identity_id): BaseResponse {
        $url = '/v1/api-key/identities/' . $identity_id . '/settings';
        return $this->get('/' . self::SERVICE_NAME . $url);
    }

    public function saveSetting($identity_id, $auto_activate_dnd = null): BaseResponse {
        $url = '/v1/api-key/identities/' . $identity_id . '/settings';
        $body = [];
        if ($auto_activate_dnd) { $body['auto_activate_dnd'] = $auto_activate_dnd; }

        return $this->patch('/' . self::SERVICE_NAME . $url, $body);
    }
}
