<?php

namespace Velcoda\Services\Http\Telebutler;

use Velcoda\Services\Http\BaseResponse;
use Velcoda\Services\Http\BaseService;

class ExtOutlook extends BaseService
{
    const SERVICE_NAME = 'ext-outlook';

    public static function client($timeout = 6): ExtOutlook
    {
        return new ExtOutlook(self::SERVICE_NAME, $timeout);
    }

    public function getStats($customer_id): BaseResponse {
        $url = '/v1/api-key/customers/' . $customer_id . '/status';
        return $this->get('/' . self::SERVICE_NAME . $url);
    }
}
