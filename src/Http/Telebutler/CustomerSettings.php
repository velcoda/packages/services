<?php

namespace Velcoda\Services\Http\Telebutler;

use Velcoda\Services\Http\BaseResponse;
use Velcoda\Services\Http\BaseService;

class CustomerSettings extends BaseService
{
    const SERVICE_NAME = 'customer-settings';

    public static function client($timeout = 6): CustomerSettings
    {
        return new CustomerSettings(self::SERVICE_NAME, $timeout);
    }

    public function loadSetting($customer_id): BaseResponse {
        $url = '/v1';
        if ($this->api_key) {
            $url .= '/api-key';
        }
        $url .= '/customers/' . $customer_id . '/settings';
        return $this->get('/' . self::SERVICE_NAME . $url);
    }

    public function saveSetting($customer_id,
                                $email_type = null,
                                $allow_sms_notifications = null,
                                $allow_vat_pin_deactivation = null,
                                $allow_chain_announcements = null,
                                $is_demo = null,
                                $allow_edit_announcement_activation_time = null,
                                $allow_starface_vta = null,
                                $allow_outlook_addin = null): BaseResponse {
        $url = '/v1';
        if ($this->api_key) {
            $url .= '/api-key';
        }
        $url .= '/customers/' . $customer_id . '/settings';

        $body = [];
        if ($email_type) { $body['email_type'] = $email_type; }
        if ($allow_sms_notifications) { $body['allow_sms_notifications'] = $allow_sms_notifications; }
        if ($allow_vat_pin_deactivation) { $body['allow_vat_pin_deactivation'] = $allow_vat_pin_deactivation; }
        if ($allow_chain_announcements) { $body['allow_chain_announcements'] = $allow_chain_announcements; }
        if ($is_demo) { $body['is_demo'] = $is_demo; }
        if ($allow_edit_announcement_activation_time) { $body['allow_edit_announcement_activation_time'] = $allow_edit_announcement_activation_time; }
        if ($allow_starface_vta) { $body['allow_starface_vta'] = $allow_starface_vta; }
        if ($allow_outlook_addin) { $body['allow_outlook_addin'] = $allow_outlook_addin; }

        return $this->patch('/' . self::SERVICE_NAME . $url, $body);
    }
}
