<?php

namespace Velcoda\Services\Http\Telebutler;

use Carbon\CarbonInterface;
use Velcoda\Exceptions\Exceptions\HTTP_BAD_REQUEST;
use Velcoda\Exceptions\Exceptions\HTTP_UNPROCESSABLE_ENTITY;
use Velcoda\Services\Http\BaseResponse;
use Velcoda\Services\Http\BaseService;

class Absences extends BaseService
{
    const SERVICE_NAME = 'absences';

    public static function client($timeout = 6): Absences
    {
        return new Absences(self::SERVICE_NAME, $timeout);
    }

    public function listAbsencesForIdentity($identity_id): BaseResponse {

        $url = '/v1';
        if ($this->api_key) {
            $url .= '/api-key';
        }
        $url .= '/identities/' . $identity_id . '/absences';

        return $this->get('/' . self::SERVICE_NAME . $url);
    }

    public function getSubstituteOptionsForIdentity($identity_id): BaseResponse {
        $url = '/v1/identities/' . $identity_id . '/substitute-options';
        return $this->get('/' . self::SERVICE_NAME . $url);
    }

    public function getActiveForIdentity($identity_id): BaseResponse {
        $url = '/v1/api-key/identities/' . $identity_id . '/active';
        return $this->get('/' . self::SERVICE_NAME . $url);
    }

    public function getSubstituteOptionsForIdentityApi($identity_id): BaseResponse {
        $url = '/v1/api-key/identities/' . $identity_id . '/substitute-options';
        return $this->get('/' . self::SERVICE_NAME . $url);
    }

    public function getSettingsForIdentity($identity_id): BaseResponse {
        $url = '/v1';
        if ($this->api_key) {
            $url .= '/api-key';
        }
        $url .= '/identities/' . $identity_id . '/settings';
        return $this->get('/' . self::SERVICE_NAME . $url);
    }

    public function updateSettingsForIdentity($identity_id, $standard_substitute, $absence_substitute): BaseResponse {
        $url = '/v1';
        if ($this->api_key) {
            $url .= '/api-key';
        }
        $url .= '/identities/' . $identity_id . '/settings';

        $body = [];
        if ($standard_substitute) {
            $body['standard_substitute_option_id'] = $standard_substitute;
        }
        if ($absence_substitute) {
            $body['absence_substitute_option_id'] = $absence_substitute;
        }
        return $this->patch('/' . self::SERVICE_NAME . $url, $body);
    }

    public function createSubstituteOptionForIdentityByFileId($identity_id, $title, $file_id): BaseResponse {
        $url = '/v1/api-key/identities/' . $identity_id . '/substitute-options';
        $body = [
            'title' => $title,
            'file_id' => $file_id
        ];
        return $this->post('/' . self::SERVICE_NAME . $url, $body);
    }

    public function deleteSubstituteOptionForIdentityByFileId($identity_id, $file_id): BaseResponse {
        $url = '/v1/api-key/identities/' . $identity_id . '/substitute-options?file_id=' . $file_id;
        return $this->delete('/' . self::SERVICE_NAME . $url);
    }

    public function checkForOverlapsApi(string $identity_id, int $start_time, int $end_time, string|null $excluded_absence_id = null): BaseResponse {
        $url = '/v1/api-key/identities/' . $identity_id . '/absences/check-for-overlap';
        if ($excluded_absence_id) {
            $url .= '?excluded=' . $excluded_absence_id;
        }
        $body = [
            'start_time' => $start_time,
            'end_time' => $end_time,
        ];
        return $this->post('/' . self::SERVICE_NAME . $url, $body);
    }

    public function checkForOverlaps(string $identity_id, int $start_time, int $end_time, string|null $excluded_absence_id = null): BaseResponse {
        $url = '/v1/identities/' . $identity_id . '/absences/check-for-overlap';
        if ($excluded_absence_id) {
            $url .= '?excluded=' . $excluded_absence_id;
        }
        $body = [
            'start_time' => $start_time,
            'end_time' => $end_time,
        ];
        return $this->post('/' . self::SERVICE_NAME . $url, $body);
    }
    public function getCurrentAnnouncementsByCustomerId(string $customer_id): BaseResponse {
        $url = '/v1';
        if ($this->api_key) {
            $url .= '/api-key';
        }
        $url .= '/customers/' . $customer_id . '/current-announcements';

        return $this->get('/' . self::SERVICE_NAME . $url);
    }

    public function getCurrentAnnouncementByIdentityId(string $identity_id): BaseResponse {
        $url = '/v1';
        if ($this->api_key) {
            $url .= '/api-key';
        }
        $url .= '/identities/' . $identity_id . '/current-announcement';

        return $this->get('/' . self::SERVICE_NAME . $url);
    }

    public function createAbsence(string $identity_id, string $title, CarbonInterface $start_time, CarbonInterface $end_time, bool $activated, array $substitutes, bool $explicit_time_configuration = false): BaseResponse {
        $subs = [];
        foreach ($substitutes as $substitute) {
            if (!key_exists('start_time', $substitute) || !key_exists('end_time', $substitute) || !key_exists('substitute_option_id', $substitute)) {
                throw new HTTP_BAD_REQUEST();
            }
            $subs[] = [
                'start_time' => $substitute['start_time'],
                'end_time' => $substitute['end_time'],
                'substitute_option_id' => $substitute['substitute_option_id'],
            ];
        }
        $body = [
            'title' => $title,
            'start_time' => $start_time->timestamp,
            'end_time' => $end_time->timestamp,
            'activated' => $activated,
            'substitutes' => $subs,
            'explicit_time_configuration' => $explicit_time_configuration
        ];
        $url = '/v1/api-key/identities/' . $identity_id . '/absences';
        return $this->post('/' . self::SERVICE_NAME . $url, $body);
    }

    public function upsertAbsence(string $absence_id, string $identity_id, string $title, CarbonInterface $start_time, CarbonInterface $end_time, bool $activated, array $substitutes, bool $explicit_time_configuration = false): BaseResponse {
        $subs = [];
        foreach ($substitutes as $substitute) {
            if (!key_exists('start_time', $substitute) || !key_exists('end_time', $substitute) || !key_exists('substitute_option_id', $substitute)) {
                throw new HTTP_BAD_REQUEST();
            }
            $subs[] = [
                'start_time' => $substitute['start_time'],
                'end_time' => $substitute['end_time'],
                'substitute_option_id' => $substitute['substitute_option_id'],
            ];
        }
        $body = [
            'title' => $title,
            'start_time' => $start_time->timestamp,
            'end_time' => $end_time->timestamp,
            'activated' => $activated,
            'substitutes' => $subs,
            'explicit_time_configuration' => $explicit_time_configuration
        ];
        $url = '/v1/api-key/identities/' . $identity_id . '/absences/' . $absence_id;
        return $this->patch('/' . self::SERVICE_NAME . $url, $body);
    }

    public function deleteAbsence(string $absence_id, string $identity_id): BaseResponse {
        $url = '/v1/api-key/identities/' . $identity_id . '/absences/' . $absence_id;
        return $this->delete('/' . self::SERVICE_NAME . $url);
    }
}
