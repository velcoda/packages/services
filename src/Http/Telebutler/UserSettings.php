<?php

namespace Velcoda\Services\Http\Telebutler;

use Velcoda\Services\Http\BaseResponse;
use Velcoda\Services\Http\BaseService;

class UserSettings extends BaseService
{
    const SERVICE_NAME = 'user-settings';

    public static function client($timeout = 6): UserSettings
    {
        return new UserSettings(self::SERVICE_NAME, $timeout = 6);
    }

    public function loadSetting($identity_id): BaseResponse {
        $url = '/v1/api-key/identities/' . $identity_id . '/settings';
        return $this->get('/' . self::SERVICE_NAME . $url);
    }

    public function saveSetting($identity_id,
                                $email_type = null,
                                $send_voice_call_sms = null,
                                $send_voice_mail_sms = null,
                                $send_voice_call_email = null,
                                $send_voice_mail_email = null,
                                $use_chain_announcement = null,
                                $manage_user = null,
                                $welcome_mail_send_time = null,
                                $instructions_mail_send_time = null): BaseResponse {
        $url = '/v1/api-key/identities/' . $identity_id . '/settings';
        $body = [];
        if ($email_type) { $body['email_type'] = $email_type; }
        if ($send_voice_call_sms !== null) { $body['send_voice_call_sms'] = $send_voice_call_sms; }
        if ($send_voice_mail_sms !== null) { $body['send_voice_mail_sms'] = $send_voice_mail_sms; }
        if ($send_voice_call_email !== null) { $body['send_voice_call_email'] = $send_voice_call_email; }
        if ($send_voice_mail_email !== null) { $body['send_voice_mail_email'] = $send_voice_mail_email; }
        if ($use_chain_announcement !== null) { $body['use_chain_announcement'] = $use_chain_announcement; }
        if ($manage_user !== null) { $body['manage_user'] = $manage_user; }
        if ($welcome_mail_send_time) { $body['welcome_mail_send_time'] = $welcome_mail_send_time; }
        if ($instructions_mail_send_time) { $body['instructions_mail_send_time'] = $instructions_mail_send_time; }

        return $this->patch('/' . self::SERVICE_NAME . $url, $body);
    }
}
