<?php

namespace Velcoda\Services\Http;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;
use Velcoda\Exceptions\Exceptions\HTTP_BAD_REQUEST;
use Velcoda\Exceptions\Exceptions\HTTP_CONFLICT;
use Velcoda\Exceptions\Exceptions\HTTP_FORBIDDEN;
use Velcoda\Exceptions\Exceptions\HTTP_INTERNAL_SERVER;
use Velcoda\Exceptions\Exceptions\HTTP_NOT_FOUND;
use Velcoda\Exceptions\Exceptions\HTTP_PAYMENT_REQUIRED;
use Velcoda\Exceptions\Exceptions\HTTP_TOO_MANY_REQUESTS;
use Velcoda\Exceptions\Exceptions\HTTP_UNAUTHORIZED;
use Velcoda\Exceptions\Exceptions\HTTP_UNPROCESSABLE_ENTITY;

class BaseService
{
    private VelcodaHttpClient $client;
    protected ?string $api_key = null;
    protected ?string $access_token = null;

    public function __construct($service_name, $timeout = 6)
    {
        $current_app = env('APP_NAME');
        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'User-Agent' => 'velcoda/1.0 (service: ' . $current_app . ')',
        ];
        $this->client = new VelcodaHttpClient([
            'base_uri' => self::getHostname($service_name),
            'headers' => $headers,
            'timeout' => $timeout,
            'connect_timeout' => $timeout
        ]);
    }

    private static function getHostname($service_name) {
        $env = env('APP_ENV');
        if (!$env) {
            throw new HTTP_INTERNAL_SERVER('APP_ENV not set!');
        }
        if ($env === 'production' || $env === 'staging') {
            return 'http://' . $service_name . '-service.' . $service_name . '.svc.cluster.local';
        }
        $service_name = str_replace('-', '_', $service_name);
        return env(strtoupper($service_name) . '_HOST');
    }

    public function setApiKey(): self {
        $this->api_key = env('VELCODA_ACCESS_KEY');
        return $this;
    }

    public function setAccessToken($access_token): self {
        $this->access_token = $access_token;
        return $this;
    }

    /**
     * @param $uri
     * @param array $headers
     * @return BaseResponse|void
     * @throws GuzzleException
     * @throws HTTP_BAD_REQUEST
     * @throws HTTP_CONFLICT
     * @throws HTTP_FORBIDDEN
     * @throws HTTP_INTERNAL_SERVER
     * @throws HTTP_NOT_FOUND
     * @throws HTTP_PAYMENT_REQUIRED
     * @throws HTTP_UNAUTHORIZED
     * @throws HTTP_UNPROCESSABLE_ENTITY
     */
    public function get($uri, $headers = []) {
        try {
            $this->mergeAuthHeaders($headers);
            $options = [
                'headers' => $headers,
            ];
            return new BaseResponse($this->client->get($uri, $options));

        } catch (ClientException $e) {
            $this->throw($e);
        }
    }

    /**
     * @param $uri
     * @param array $body
     * @param array $headers
     * @return BaseResponse|void
     * @throws GuzzleException
     * @throws HTTP_BAD_REQUEST
     * @throws HTTP_CONFLICT
     * @throws HTTP_FORBIDDEN
     * @throws HTTP_INTERNAL_SERVER
     * @throws HTTP_NOT_FOUND
     * @throws HTTP_PAYMENT_REQUIRED
     * @throws HTTP_UNAUTHORIZED
     * @throws HTTP_UNPROCESSABLE_ENTITY
     */
    public function post($uri, $body = [], $headers = []) {
        try {
            $this->mergeAuthHeaders($headers);
            $options = [
                'headers' => $headers,
                'body' => json_encode($body)
            ];
            return new BaseResponse($this->client->post($uri, $options));
        } catch (ClientException $e) {
            $this->throw($e);
        }
    }

    /**
     * @param $uri
     * @param array $body
     * @param array $headers
     * @return BaseResponse|void
     * @throws GuzzleException
     * @throws HTTP_BAD_REQUEST
     * @throws HTTP_CONFLICT
     * @throws HTTP_FORBIDDEN
     * @throws HTTP_INTERNAL_SERVER
     * @throws HTTP_NOT_FOUND
     * @throws HTTP_PAYMENT_REQUIRED
     * @throws HTTP_UNAUTHORIZED
     * @throws HTTP_UNPROCESSABLE_ENTITY
     */
    public function patch($uri, $body = [], $headers = []) {
        try {
            $this->mergeAuthHeaders($headers);
            $options = [
                'headers' => $headers,
                'body' => json_encode($body)
            ];
            return new BaseResponse($this->client->patch($uri, $options));
        } catch (ClientException $e) {
            $this->throw($e);
        }
    }

    /**
     * @param $uri
     * @param array $body
     * @param array $headers
     * @return BaseResponse|void
     * @throws GuzzleException
     * @throws HTTP_BAD_REQUEST
     * @throws HTTP_CONFLICT
     * @throws HTTP_FORBIDDEN
     * @throws HTTP_INTERNAL_SERVER
     * @throws HTTP_NOT_FOUND
     * @throws HTTP_PAYMENT_REQUIRED
     * @throws HTTP_UNAUTHORIZED
     * @throws HTTP_UNPROCESSABLE_ENTITY
     */
    public function put($uri, $body = [], $headers = []) {
        try {
            $this->mergeAuthHeaders($headers);
            $options = [
                'headers' => $headers,
                'body' => json_encode($body)
            ];
            return new BaseResponse($this->client->put($uri, $options));
        } catch (ClientException $e) {
            $this->throw($e);
        }
    }

    /**
     * @throws HTTP_PAYMENT_REQUIRED
     * @throws HTTP_CONFLICT
     * @throws HTTP_NOT_FOUND
     * @throws HTTP_UNAUTHORIZED
     * @throws HTTP_BAD_REQUEST
     * @throws HTTP_FORBIDDEN
     * @throws HTTP_INTERNAL_SERVER
     * @throws GuzzleException
     */
    public function delete($uri, $headers = []) {
        try {
            $this->mergeAuthHeaders($headers);
            $options = [
                'headers' => $headers
            ];
            return new BaseResponse($this->client->delete($uri, $options));
        } catch (ClientException $e) {
            $this->throw($e);
        }
    }

    public function request($method, $path, $headers, $body = [], $content_type = null) {
        $this->mergeAuthHeaders($headers);
        $data = [
            'headers' => $headers
        ];
        if ($content_type === 'application/json') {
            $data['json'] = $body;
        } else if ($content_type === 'multipart/form-data') {
            unset($data['headers']['content-type']); // reset header, so that boundaries can be calculated again
            $data['multipart'] = $body;
        } else if ($content_type === 'application/x-www-form-urlencoded') {
            $data['form_params'] = $body;
        } else {
            $data['body'] = json_encode($body);
        }
        try {
            return new BaseResponse($this->client->request($method, $path, $data));
        } catch (ClientException $e){
            $this->throw($e);
        } catch(ConnectException $e) {
            // 403 when too many attempts
            if ($e->getCode() === 403) {
                throw new HTTP_UNAUTHORIZED();
            }
        }
    }

    public function requestAsync($method, $path, $headers, $body = [], $content_type = null) {
        $this->mergeAuthHeaders($headers);
        $data = [
            'headers' => $headers
        ];
        if ($content_type === 'application/json') {
            $data['json'] = $body;
        } else if ($content_type === 'multipart/form-data') {
            unset($data['headers']['content-type']); // reset header, so that boundaries can be calculated again
            $data['multipart'] = $body;
        } else if ($content_type === 'application/x-www-form-urlencoded') {
            $data['form_params'] = $body;
        } else {
            $data['body'] = json_encode($body);
        }
        return $this->client->requestAsync($method, $path, $data);
    }

    private function mergeAuthHeaders(&$headers): void
    {
        if (isset($this->access_token)) {
            $headers['Authorization'] = 'Bearer ' . $this->access_token;
        }
        if (isset($this->api_key)) {
            $headers['x-api-key'] = $this->api_key;
        }
    }

    /**
     * @throws HTTP_PAYMENT_REQUIRED
     * @throws HTTP_CONFLICT
     * @throws HTTP_NOT_FOUND
     * @throws HTTP_UNAUTHORIZED
     * @throws HTTP_BAD_REQUEST
     * @throws HTTP_INTERNAL_SERVER
     * @throws HTTP_FORBIDDEN
     * @throws HTTP_UNPROCESSABLE_ENTITY
     */
    private function throw(ClientException $e) {
        $message = json_decode($e->getResponse()->getBody()->getContents());
        if (property_exists($message, 'details')) {
            $message = $message->details;
        } else {
            $message = $message->message;
        }
        throw match ($e->getCode()) {
            400 => new HTTP_BAD_REQUEST($message),
            401 => new HTTP_UNAUTHORIZED($message),
            402 => new HTTP_PAYMENT_REQUIRED($message),
            403 => new HTTP_FORBIDDEN($message),
            404 => new HTTP_NOT_FOUND($message),
            409 => new HTTP_CONFLICT($message),
            422 => new HTTP_UNPROCESSABLE_ENTITY($message),
            429 => new HTTP_TOO_MANY_REQUESTS($message),
            500 => new HTTP_INTERNAL_SERVER($message),
            default => $e,
        };
    }
}
